﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ParkingController : ControllerBase
    {
        
        private readonly IParkingService _parkingService;

        public ParkingController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [Route("balance")]
        [HttpGet]
        public async Task<ActionResult> GetBalance()
        {
            return Ok(_parkingService.GetBalance());           

        }

        [Route("capacity")]
        [HttpGet]
        public async Task<ActionResult> GetCapacity()
        {
            return Ok(_parkingService.GetCapacity());

        }

        [Route("freePlaces")]
        [HttpGet]
        public async Task<ActionResult> GetFreePlaces()
        {
            return Ok(_parkingService.GetFreePlaces());

        }


    }
}
