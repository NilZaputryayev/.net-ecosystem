﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.WebAPI.DTO;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : ControllerBase
    {

        private readonly IParkingService _parkingService;

        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet("last")]
        public async Task<ActionResult> GetLastTransactions()
        {
            
            return Ok(_parkingService.GetLastParkingTransactions());
        }

        [HttpGet("all")]
        public async Task<ActionResult> GetAllTransactions()
        {
            try
            {
                return Ok(_parkingService.ReadFromLog());
            }
            catch
            {
                return NotFound(new { message = "Transaction file not ready yet" });
            }

        }

        [HttpPut("topUpVehicle")]
        public async Task<ActionResult> topUpVehicle(TopUpVehicleDTO topUpVehicleDTO)
        {
            var validator = new CoolParking.BL.Helpers.IdValidator();

            if (!validator.Validate(topUpVehicleDTO.id))
                return BadRequest(new { message = "Wrong ID" });

            if (topUpVehicleDTO.Sum < 0)
                return BadRequest(new { message = "Sum can't be negative" });

            var vehicle = _parkingService.GetVehicles().Where(x => x.Id == topUpVehicleDTO.id).FirstOrDefault();

            if (vehicle == null)
                return NotFound(new { message = "Vehicle not found" });


            _parkingService.TopUpVehicle(topUpVehicleDTO.id, topUpVehicleDTO.Sum);

            return Ok(vehicle);
            


        }


    }
}

