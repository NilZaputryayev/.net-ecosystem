﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VehiclesController : ControllerBase
    {

        private readonly IParkingService _parkingService;

        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get(string id)
        {
            var validator = new CoolParking.BL.Helpers.IdValidator();

            if (!validator.Validate(id)) return BadRequest(new { message = "Wrong ID" });

            var vehicle = _parkingService.GetVehicles().Where(x => x.Id == id).FirstOrDefault();

            if (vehicle == null) return NotFound(new { message = "Vehicle not found" });

            return Ok(vehicle);
        }

        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            return Ok(_parkingService.GetVehicles());

        }

        [HttpPost]
        public async Task<ActionResult> AddVehicle(Vehicle vehicle)
        {
            try
            {
                _parkingService.AddVehicle(vehicle);
                return CreatedAtRoute(string.Empty, vehicle);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }


        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteVehicle(string id)
        {
            var validator = new CoolParking.BL.Helpers.IdValidator();

            if (!validator.Validate(id)) return BadRequest(new { message = "Wrong ID" });

            var vehicle = _parkingService.GetVehicles().Where(x => x.Id == id).FirstOrDefault();

            if(vehicle == null) return NotFound(new { message = "Vehicle not found" });

            try
            {
                _parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(new { message = ex.Message });
            }

        }
    }
}

