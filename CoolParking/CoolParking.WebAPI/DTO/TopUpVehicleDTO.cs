﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.DTO
{
    public class TopUpVehicleDTO
    {
        public string id { get; set; }
        [JsonNumberHandlingAttribute(JsonNumberHandling.Strict)]
        [Range(0, Double.MaxValue, ErrorMessage = "Must be positive")]
        public decimal Sum { get; set; }
    }


}
