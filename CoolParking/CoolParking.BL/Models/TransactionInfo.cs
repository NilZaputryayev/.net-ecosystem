﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;
using System.Globalization;
using System.Text.Json.Serialization;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        
        [JsonPropertyName("vehicleId")]
        public string VehicleID { get; set; }
        [JsonPropertyName("sum")]
        public decimal Sum { get; set; }
        [JsonPropertyName("transactionDate")]
        public DateTime Date { get; set; }
        public override string ToString() { return $@"{Date.ToString("MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture)}: {Sum} money withdrawn from vehicle with Id ='{VehicleID}'"; }
    }
}
