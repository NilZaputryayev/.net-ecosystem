﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public sealed class Parking: IDisposable
    {
        private static readonly Lazy<IDictionary<string, Vehicle>> lazy =
                new Lazy<IDictionary<string, Vehicle>>(() => new Dictionary<string, Vehicle>());

        public static IDictionary<string, Vehicle> Vehicles { get { return lazy.Value; } }

        private static decimal balance;

        public static decimal Balance
        {
            get { return balance; }
            set { balance = value; }
        }

        private Parking()
        {
        }

        public void Dispose()
        {
            Vehicles.Clear();
            Parking.Balance = 0;
        }
    }
}
