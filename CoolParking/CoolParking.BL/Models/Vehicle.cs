﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using CoolParking.BL.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using static CoolParking.BL.Helpers.RandomHelper;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {


        [Required]
        [JsonPropertyName("id")]
        [RegularExpression(@"\b[A-Z]{2}-[\d]{4}-[A-Z]{2}\b", ErrorMessage = "Incorrect ID")]
        public string Id { get; }

        [Required]
        [JsonPropertyName("vehicleType")]
        [EnumDataType(typeof(VehicleType))]
        public VehicleType VehicleType { get; }

        [Required]
        [JsonPropertyName("balance")]
        [Range(0, Double.MaxValue, ErrorMessage = "Must be positive")]
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            //if (isValidID(id) && isValidType(vehicleType) && isValidBalance(balance))
            //{
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
            //}
            //else throw new ArgumentException("Invalid vehicle object");
        }
        public override string ToString() { return $"{Id} {VehicleType.ToString()} {Balance}"; }


        public bool isValidID(string id)
        {
            var validator = new IdValidator();
            return validator.Validate(id);
        }

        private bool isValidType(VehicleType vehicleType)
        {
            var validator = new VehicleTypeValidator();
            return validator.Validate(vehicleType.ToString());
        }

        private bool isValidBalance(decimal balance)
        {
            var validator = new SumValidator();
            return validator.Validate(balance.ToString());
        }



        //Generate ID формата ХХ-YYYY-XX (где X - любая буква английского алфавита в верхнем регистре, а Y - любая цифра, например DV-2345-KJ).
        public static string GenerateRandomRegistrationPlateNumber()
        {

            string StringPrefix = GetRandomString(2);

            string StringSuffix = GetRandomString(2);

            string Digits = GetRandomStringFromDigits(4);

            string id = $"{StringPrefix}-{Digits}-{StringSuffix}";

            return id;
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            Regex regex = new Regex(@"\b[A-Z]{2}-[\d]{4}-[A-Z]{2}\b");
            var res = regex.Match(Id).Success;
            if (res == false)
                errors.Add(new ValidationResult("Incorrect Id format"));

            if (Balance < 0)
                errors.Add(new ValidationResult("Balance must be positive"));

            bool result = Enum.TryParse(typeof(VehicleType), VehicleType.ToString(), out object vehicle);
            if (result == false)
                errors.Add(new ValidationResult("Incorrect vehicle type"));

            return errors;
        }
    }
}
