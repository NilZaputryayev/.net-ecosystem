﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

//Начальный баланс Паркинга - 0;
//Вместимость Паркинга - 10;
//Период списания оплаты, N-секунд - 5;
//Период записи в лог, N-секунд - 60;
//Тарифы в зависимости от Тр.средства: Легковое - 2, Грузовое - 5, Автобус - 3.5, Мотоцикл - 1;
//Коэффициент штрафа - 2.5.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static readonly decimal StartBalance = 0;

        public static readonly int ParkingCapacity = 10;

        public static readonly int WithdrawTimerInterval = 5000;

        public static readonly int LogTimerInterval = 60000;

        public static readonly Dictionary<VehicleType, decimal> Prices = new Dictionary<VehicleType, decimal>()
        {
            {VehicleType.PassengerCar, 2},
            {VehicleType.Truck, 5},
            {VehicleType.Bus, 3.5m },
            {VehicleType.Motorcycle, 1}
        };

        public static readonly decimal Penalty = 2.5m;

    }
}
