﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService, IDisposable
    {
        readonly ITimerService _withdrawTimer;
        readonly ITimerService _logTimer;
        readonly ILogService _logService;
        readonly List<TransactionInfo> currentTransactions = new List<TransactionInfo>();




        private bool disposedValue;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;

            _withdrawTimer.Elapsed += withdrawTimer_Elapsed;
            _withdrawTimer.Start();

            _logTimer.Elapsed += logTimer_Elapsed;
            _logTimer.Start();

        }

        private void logTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (currentTransactions.Count == 0)
            {
                //for homework 2 tests only
                //_logService.Write("No cars on parking.");
                return;
            }

            for (int i = 0; i < currentTransactions.Count(); i++)
            {
                _logService.Write(currentTransactions[i].ToString());
            }

            //Can be dangerous when collection changing !!!

            //foreach (var tran in currentTransactions)
            //{
            //    _logService.Write(tran.ToString());
            //}


            currentTransactions.Clear();


        }

        private void withdrawTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            foreach (var item in Parking.Vehicles)
            {
                var vehicle = item.Value;

                var sumByVehicleType = Settings.Prices[vehicle.VehicleType];
                bool overdraft = sumByVehicleType > vehicle.Balance;
                var sumToWithdraw = sumByVehicleType;

                if (overdraft)
                {                
                   var sumRemains = vehicle.Balance > 0 ? vehicle.Balance : 0;
                   var penaltySum = (sumByVehicleType - sumRemains) * Settings.Penalty;
                   sumToWithdraw = sumRemains + penaltySum;
                }

                vehicle.Balance -= sumToWithdraw;
                Parking.Balance += sumToWithdraw;

                TransactionInfo tran = new TransactionInfo() { Date = DateTime.Now, VehicleID = vehicle.Id, Sum = sumToWithdraw };
                currentTransactions.Add(tran);
                
            }
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (GetFreePlaces() > 0)
            {
                if (!Parking.Vehicles.ContainsKey(vehicle.Id))
                {
                    Parking.Vehicles.Add(vehicle.Id, vehicle);
                }
                else throw new ArgumentException("Can't add duplicate Id");
            }
            else throw new InvalidOperationException("Parking is full");

        }



        public decimal GetBalance()
        {
            return Parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.ParkingCapacity;
        }

        public int GetFreePlaces()
        {
            int freePlaces = Settings.ParkingCapacity - Parking.Vehicles.Count;

            return freePlaces;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return currentTransactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            var vehicles = new ReadOnlyCollection<Vehicle>(Parking.Vehicles.Values.ToList());
            return vehicles;
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (Parking.Vehicles.TryGetValue(vehicleId, out Vehicle vehicle))
            {
                if (vehicle.Balance >= 0)
                {
                    Parking.Vehicles.Remove(vehicle.Id);
                }
                else throw new InvalidOperationException("Can't remove vehicle with negative balance");
            }
            else throw new ArgumentException("Vehicle not found");

        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0) throw new ArgumentException("Sum can't be negative");

            if (Parking.Vehicles.TryGetValue(vehicleId, out Vehicle vehicle))
            {
                vehicle.Balance += sum;
            }
            else throw new ArgumentException("Vehicle not found");
        }



        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Parking.Vehicles.Clear();
                    Parking.Balance = 0;
                    _withdrawTimer.Dispose();
                    _logTimer.Dispose();
                }

                disposedValue = true;
            }
        }


        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }






    }
}
