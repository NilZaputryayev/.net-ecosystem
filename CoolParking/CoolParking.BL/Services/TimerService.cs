﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer timer { get; }
        public double Interval { get; set; }

        public TimerService()
        {
            timer = new Timer();

        }

        public TimerService(double interval)
        {
            Interval = interval;
            timer = new Timer(interval);
        }

        public void FireElapsedEvent(object sender, ElapsedEventArgs e)
        {
            Elapsed?.Invoke(this, null);
        }

        public event ElapsedEventHandler Elapsed;

        public void Dispose()
        {
            timer.Dispose();
        }

        public void Start()
        {            
            timer.Elapsed += FireElapsedEvent;
            timer.Interval = Interval;
            timer.Start();
        }


        public void Stop()
        {
            timer.Stop();
        }



    }
}
