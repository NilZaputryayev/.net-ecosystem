﻿using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CoolParking.BL.Helpers
{
    public abstract class Validator
    {
        public string ErrorMessage { get; set; }
        public abstract bool Validate(string value);

    }

    public class IdValidator : Validator
    {
        public override bool Validate(string id)
        {
            Regex regex = new Regex(@"\b[A-Z]{2}-[\d]{4}-[A-Z]{2}\b");
            var res = regex.Match(id).Success;            
            ErrorMessage = res ? string.Empty : "Wrong Id";
            return res;
        }
    }

    public class SumValidator : Validator
    {
        public override bool Validate(string sum)
        {
            bool result = false;

            if (string.IsNullOrEmpty(sum))
            {
                ErrorMessage = "Sum is empty";
            }
            else if (!decimal.TryParse(sum, out decimal res))
            {
                ErrorMessage = "Sum must be decimal value";
            }
            else if (res < 0) ErrorMessage = "Sum must be positive";
            else result = true;

            return result;

        }
    }

    public class VehicleTypeValidator : Validator
    {
        public override bool Validate(string vehicleType)
        {
            bool result = Enum.TryParse(typeof(VehicleType), vehicleType, out object vehicle);
            if (result == false ) ErrorMessage = "Incorrect type of vehicle";

            return result;

        }
    }


}
