﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.BL.Helpers
{
    class RandomHelper
    {

        readonly static Random _random = new Random();

        public static string GetRandomString(int stringLen)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            for (int i = 0; i < stringLen; i++)
            {
                sb.Append((char)('A' + _random.Next(26)));
            }

            return sb.ToString();
        }

        internal static string GetRandomStringFromDigits(int len)
        {
            int maxVal = (int)Math.Pow(10, len) - 1;
            string format = $"d{maxVal.ToString().Length}";

            return _random.Next(maxVal).ToString(format);
        }
    }

}
