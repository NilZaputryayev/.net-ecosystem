﻿using CoolParking.BL.Helpers;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using CoolParking.WebAPI.DTO;

namespace WebClient
{
    class Program
    {
        private static readonly HttpClient client = new HttpClient();

        readonly static string BaseUrl = "https://localhost:44360/api/";

        readonly static string BalanceEndpoint = "parking/balance";
        readonly static string CapacityEndpoint = "parking/capacity";
        readonly static string FreePlacesEndpoint = "parking/freePlaces";
        readonly static string VehiclesEndpoint = "vehicles";
        readonly static string LastTransavtionsEndpoint = "transactions/last";
        readonly static string AllTransactionEndpoint = "transactions/all";
        readonly static string TopUpEndpoint = "transactions/topUpVehicle";




        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Blue;

            ConfigClient();

            bool showMenu = true;
            while (showMenu)
            {
                showMenu = ParkingMenu();
                Console.WriteLine("Press any key");
                Console.ReadKey();
            }
        }

        private static bool ParkingMenu()
        {

            Console.Clear();

            Console.WriteLine("Select an option:");
            Console.WriteLine("1. Show parking balance");
            Console.WriteLine("2. Show parking capacity");
            Console.WriteLine("3. Show free places count");
            Console.WriteLine("4. Show all vehicles");
            Console.WriteLine("5. Show vehicle by ID");
            Console.WriteLine("6. Add vehicle ");
            Console.WriteLine("7. Remove vehicle ");
            Console.WriteLine("8. Show current transactions");
            Console.WriteLine("9. Show all transactions");
            Console.WriteLine("10. Top up vehicle");
            Console.WriteLine("0. Exit");
            Console.Write("\r\nSelect an option: ");

            switch (Console.ReadLine())
            {
                case "1":
                    ShowInfo(BalanceEndpoint, "Parking balance is");
                    return true;
                case "2":
                    ShowInfo(CapacityEndpoint, "Parking capacity is");
                    return true;
                case "3":
                    ShowInfo(FreePlacesEndpoint, "Count of free places is");
                    return true;
                case "4":
                    ShowInfo(VehiclesEndpoint, "Vehicles");
                    return true;
                case "5":
                    ShowVehicleByID();
                    return true;
                case "6":
                    AddVehicle();
                    return true;
                case "7":
                    RemoveVehicle();
                    return true;
                case "8":
                    ShowInfo(LastTransavtionsEndpoint, "Last transactions");
                    return true;
                case "9":
                    ShowInfo(AllTransactionEndpoint, "All transactions");
                    return true;
                case "10":
                    TopUpVehicle();
                    return true;
                case "0":
                    return false;
                default:
                    return true;
            }

        }

        private static async Task RemoveVehicle()
        {
            var vehicleID = GetStringFromConsole("Enter vehicleID:");
            try
            {

                var response = await client.DeleteAsync($"{VehiclesEndpoint}/{vehicleID}");
                WriteResponse(response.ToString());
                string responseBody = await response.Content.ReadAsStringAsync();
                WriteContent(responseBody);


            }
            catch (HttpRequestException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

            }

        }


        private static string GetStringFromConsole(string message)
        {
            string result;
            do
            {
                Console.WriteLine($"Enter {message}:");
                result = Console.ReadLine();

            } while (string.IsNullOrEmpty(result));

            return result;
        }

        private static async Task AddVehicle()
        {
            var vehicleID = GetStringFromConsole("vehicleID:");
            var sumValidator = new SumValidator();
            var sum = GetStringFromConsole("balance:");
            var vehicleType = GetStringFromConsole("vehicle type(1-4):");

            if (decimal.TryParse(sum, out decimal sumDecimal))
            {
                if (int.TryParse(vehicleType, out int vehicleTYpeInt))
                {

                    var json = JsonSerializer.Serialize(new { id = vehicleID, vehicleType = vehicleTYpeInt, balance = sumDecimal });
                    var data = new StringContent(json, Encoding.UTF8, "application/json");

                    try
                    {

                        var response = await client.PostAsync(VehiclesEndpoint, data);
                        WriteResponse(response.ToString());
                        string responseBody = await response.Content.ReadAsStringAsync();
                        WriteContent(responseBody);


                    }
                    catch (HttpRequestException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);

                    }
                }
                else Console.WriteLine("VehicleType must be integer");
            }
            else Console.WriteLine("Sum must be decimal");

        }

        private static async Task ShowVehicleByID()
        {
            var vehicleID = GetStringFromConsole("Enter vehicleID:");
            await ShowInfo($"{VehiclesEndpoint}/{vehicleID}", $"Vehicle '{vehicleID}' info:");
        }

        private static async Task TopUpVehicle()
        {
            var vehicleID = GetStringFromConsole("Enter vehicleID:");
            Console.WriteLine("Enter top up sum");
            var sum = Console.ReadLine();

            if (decimal.TryParse(sum, out decimal sumDecimal))
            {

                TopUpVehicleDTO TopUpVehicleItem = new TopUpVehicleDTO() { id = vehicleID, Sum = sumDecimal };
                //var options = new JsonSerializerOptions
                //{
                //    WriteIndented = true,
                //};
                var json = JsonSerializer.Serialize(TopUpVehicleItem);
                var data = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {

                    var response = await client.PutAsync(TopUpEndpoint, data);
                    WriteResponse(response.ToString());
                    string responseBody = await response.Content.ReadAsStringAsync();
                    WriteContent(responseBody);

                }
                catch (HttpRequestException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);

                }
            }
            else Console.WriteLine("Sum must be decimal");
        }

        private static void ConfigClient()
        {
            client.BaseAddress = new Uri(BaseUrl);
        }

        private static async Task ShowInfo(string Endpoint, string message)
        {
            try
            {
                Console.WriteLine($"{message}: { await GetResponce(Endpoint) }");
            }
            catch (HttpRequestException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

            }
        }

        private static async Task<string> GetResponce(string Endpoint)
        {
            var response = await client.GetAsync(Endpoint);
            WriteResponse(response.ToString());
            string responseBody = await response.Content.ReadAsStringAsync();
            WriteContent(responseBody);
            return responseBody;
        }

        private static void WriteContent(string responseBody)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine($"Content : {responseBody}");
            Console.WriteLine("-------------------------");
            Console.ForegroundColor = ConsoleColor.White;
        }

        private static void WriteResponse(string data)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Server response: { data.ToString() }");
            Console.ForegroundColor = ConsoleColor.White;


        }
    }
}
