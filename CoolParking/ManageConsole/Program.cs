﻿using CoolParking.BL.Helpers;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Timers;

namespace ManageConsole
{
    class Program
    {
        static ParkingService _parkingService;
        static void Main(string[] args)
        {
            var _withdrawTimer = new TimerService(Settings.WithdrawTimerInterval);
            var _logTimer = new TimerService() { Interval = Settings.LogTimerInterval };
            var _logService = new LogService("Transaction.log");
            _parkingService = new ParkingService(_withdrawTimer, _logTimer, _logService);

        //    _parkingService.AddVehicle(new Vehicle("AA-1111-BBB", VehicleType.PassengerCar, -10));

            bool showMenu = true;
            while (showMenu)
            {
                showMenu = ParkingMenu();
                Console.WriteLine("Press any key");
                Console.ReadKey();
            }


        }

        private static bool ParkingMenu()
        {
            Console.Clear();

            Console.WriteLine("Select an option:");
            Console.WriteLine("1. Show parking balance");
            Console.WriteLine("2. Show current benefit");
            Console.WriteLine("3. Show parking status");
            Console.WriteLine("4. Show current transactions");
            Console.WriteLine("5. Show all transactions");
            Console.WriteLine("6. Show all vehicles");
            Console.WriteLine("7. Add vehicle");
            Console.WriteLine("8. Remove vehicle from parking");
            Console.WriteLine("9. Top up vehicle");
            Console.WriteLine("0. Exit");
            Console.Write("\r\nSelect an option: ");

            switch (Console.ReadLine())
            {
                case "1":
                    ShowParkingBalance();
                    return true;
                case "2":
                    ShowCurrentBenefit();
                    return true;
                case "3":
                    ShowParkingStatus();
                    return true;
                case "4":
                    ShowCurrentTransactions();
                    return true;
                case "5":
                    ShowAllTransactions();
                    return true;
                case "6":
                    ShowAllVehicles();
                    return true;
                case "7":
                    AddVehicle();
                    return true;
                case "8":
                    RemoveVehicle();
                    return true;
                case "9":
                    TopUpVehicle();
                    return true;
                case "0":
                    return false;
                default:
                    return true;
            }

        }

        private static void AddVehicle()
        {
            Console.Write("Enter vehicle number:");
            var id = Console.ReadLine();
            var validator = new IdValidator();
            if (validator.Validate(id))
            {
                Console.Write("Enter balance of vehicle:");
                var sum = Console.ReadLine();
                var sumValidator = new SumValidator();
                if (sumValidator.Validate(sum))
                {
                    ShowTransportMenu();
                    Console.Write("Enter VehicleType (1-4) or type name:");
                    var vehicleType = Console.ReadLine();
                    var vehicleTypeValidator = new VehicleTypeValidator();
                    if (vehicleTypeValidator.Validate(vehicleType))
                    {
                        try
                        {
                            var vehicle = new Vehicle(id, (VehicleType)Enum.Parse(typeof(VehicleType), vehicleType), Decimal.Parse(sum));
                            _parkingService.AddVehicle(vehicle);
                            Console.WriteLine($"Vehicle: {vehicle.ToString()} added.");

                        }
                        catch (ArgumentException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                    }
                    else Console.WriteLine(vehicleTypeValidator.ErrorMessage);
                }
                else Console.WriteLine(sumValidator.ErrorMessage);
            }
            else Console.WriteLine(validator.ErrorMessage);
        }



        private static void ShowParkingStatus()
        {
            var parkingCapacity = _parkingService.GetCapacity();
            var parkingFreePlaces = _parkingService.GetFreePlaces();
            Console.WriteLine($"Current parking status: free {parkingFreePlaces} from {parkingCapacity} places");


        }

        private static void ShowCurrentBenefit()
        {
            var currentTransactions = _parkingService.GetLastParkingTransactions();
            var sum = currentTransactions.Sum(tr => tr.Sum);
            Console.WriteLine($"Current transactions sum is {sum}");
        }

        private static void ShowParkingBalance()
        {
            Console.WriteLine($"Current parking balance is {Parking.Balance}");
        }

        private static void ShowCurrentTransactions()
        {
            var currentTransactions = _parkingService.GetLastParkingTransactions();
            foreach (var tran in currentTransactions)
            {
                Console.WriteLine(tran.ToString());
            }
        }

        private static void ShowAllTransactions()
        {
            try
            {
                var transactions = _parkingService.ReadFromLog();
                Console.Write(transactions);
            }
            catch(InvalidOperationException ex)
            {
                Console.Write("Transaction log not yet created.");
            }
        }

        private static void ShowAllVehicles()
        {
            if (Parking.Vehicles.Count > 0)
            {
                Console.WriteLine("VehicleID | VehicleType | Balance");
                Console.WriteLine("---------------------------------");

                foreach (var vehicle in Parking.Vehicles)
                {
                    Console.WriteLine(vehicle.Value.ToString());
                }

                Console.WriteLine("---------------------------------");
            }
            else Console.WriteLine("Parking is empty.");


        }


        private static void RemoveVehicle()
        {
            Console.Write("Enter vehicle number:");
            var id = Console.ReadLine();
            var validator = new IdValidator();
            if (validator.Validate(id))
            {
                try
                {
                    _parkingService.RemoveVehicle(id);
                    Console.WriteLine($"Vehicle: {id} removed.");

                }
                catch (ArgumentException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (InvalidOperationException ex)
                {
                    Console.WriteLine(ex.Message);
                }


            }
            else Console.WriteLine("Incorrect format of vehicleID");
        }

        private static void TopUpVehicle()
        {
            Console.Write("Enter vehicle number:");
            var id = Console.ReadLine();
            var validator = new IdValidator();
            if (validator.Validate(id))
            {
                Console.Write("Enter sum:");
                var sum = Console.ReadLine();
                var sumValidator = new SumValidator();
                if (sumValidator.Validate(sum))
                {
                    try
                    {
                        _parkingService.TopUpVehicle(id, Decimal.Parse(sum));
                        Console.WriteLine($"You succesfully top up vehicle: {id} on sum: {sum}");

                    }
                    catch (ArgumentException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }

                }
                else Console.WriteLine(sumValidator.ErrorMessage);
            }
            else Console.WriteLine(validator.ErrorMessage);
        }



        private static void ShowTransportMenu()
        {
            Console.Clear();
            Console.WriteLine("Select an option:");
            Console.WriteLine("1. PassengerCar");
            Console.WriteLine("2. Truck");
            Console.WriteLine("3. Bus");
            Console.WriteLine("4. Motorcycle");
        }

    }
}
